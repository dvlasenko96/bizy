import React from 'react'
import Autocomplete from '../../../components/Autocomplete/Autocomplete'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import BirdsService from '../../../services/Birds/Birds'
import birds from '../../../../public/birds.json';
import './HomeView.scss'


const styles = {
  block: {
    marginTop: 16,
  },
  radioButton: {
    marginBottom: 16,
    textAlign: 'left',
  },
};

export default class HomeView extends React.Component {
  birdsSerice = new BirdsService(birds);
  state = {
    algorithm: 'fuzzyMatch',
    dataSource: [],
  };

  handleChange(e, value) {
    this.setState({
      algorithm: value,
    });
  }

  handleUpdateAutocomplete(value) {
    const predictions = this.birdsSerice.predictBirds(value, this.state.algorithm);

    this.setState({
      dataSource: predictions.map(el => {
        return {
          text: el,
          value: el
        };
      })
    });
  }

  render() {
    return (
      <div>
        <Autocomplete hintText="Birds search"
          dataSource={this.state.dataSource}
          onUpdateInput={this.handleUpdateAutocomplete.bind(this)}
          />
        <div style={styles.block}>
          <RadioButtonGroup name="algorithm" 
            defaultSelected={this.state.algorithm}
            onChange={this.handleChange.bind(this)}>
            <RadioButton
              value="fuzzyMatch"
              label="Fuzzy Match"
              style={styles.radioButton}
            />
            <RadioButton
              value="fuzzySearch"
              label="Fuzzy Search"
              style={styles.radioButton}
            />
            <RadioButton
              value="bitap"
              label="Bitap (slow)"
              style={styles.radioButton}
            />
          </RadioButtonGroup>
        </div>
      </div>
    );
  }
}