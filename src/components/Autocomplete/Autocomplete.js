import React from 'react'
import AutoComplete from 'material-ui/AutoComplete'
import './Autocomplete.scss'

export default class Autocomplete extends React.Component {
  state = {
    dataSource: [],
  };

  componentWillReceiveProps(nextProps, nextState) {
    this.setState({
      dataSource: nextProps.dataSource
    });
  }

  render () {
    return (
      <div>
        <AutoComplete
          hintText={this.props.hintText}
          dataSource={this.props.dataSource}
          filter={(el) => el}
          fullWidth={true}
          onUpdateInput={this.props.onUpdateInput}
            />
      </div>
    )
  }
}
