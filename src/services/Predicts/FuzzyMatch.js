function fuzzyMatch(searchSet, query) {
  var tokens = query.toLowerCase().split(''),
    matches = [];
  searchSet.forEach(function (string) {
    var tokenIndex = 0,
      stringIndex = 0,
      matchWithHighlights = '',
      string = string.toLowerCase();
    while (stringIndex < string.length) {
      if (string[stringIndex] === tokens[tokenIndex]) {
        tokenIndex++;
        if (tokenIndex >= tokens.length) {
          matches.push({
            match: string,
          });
          break;
        }
      }
      stringIndex++;
    }
  });
  return matches;
}

export default function(data, str) {
    let results = fuzzyMatch(data, str);
    results = results.map(el => el.match);
    return results.slice(0, 5);
}
