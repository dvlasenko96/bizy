function fuzzySearch(needle, haystack) {
  var hlen = haystack.length;
  var nlen = needle.length;
  if (nlen > hlen) {
    return false;
  }
  if (nlen === hlen) {
    return needle === haystack;
  }
  outer: for (var i = 0, j = 0; i < nlen; i++) {
    var nch = needle.charCodeAt(i);
    while (j < hlen) {
      if (haystack.charCodeAt(j++) === nch) {
        continue outer;
      }
    }
    return false;
  }
  return true;
}

export default function(data, str) {
    const resultArray = [];

    for(var i = 0; i < data.length; ++i) {
            let item = data[i];
            if(str.length > item.length) continue;
            let point = fuzzySearch(str, item);
            if(point) resultArray.push(item);
    }

    return resultArray.slice(0, 5);
}
