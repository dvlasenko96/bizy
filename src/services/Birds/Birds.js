import bitap from '../Predicts/Bitap'
import damerau from '../Predicts/Damerau'
import fuzzyMatch from '../Predicts/FuzzyMatch'
import fuzzySearch from '../Predicts/FuzzySearch'

export default class BirdsService {
    constructor(data) {
        this.data = data;
    }

    predictBirds(str, algorithm) {
        if(!this.data) return [];

        switch (algorithm) {
            case 'bitap':
                return bitap(this.data, str);
                break;

            case 'fuzzyMatch':
                return fuzzyMatch(this.data, str);
                break;

            case 'fuzzySearch':
                return fuzzySearch(this.data, str);
                break;
            default:
                return fuzzyMatch(this.data, str);
                break;
        }
    }
}
